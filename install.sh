#!/bin/bash
#update
sudo echo 'Defaults        env_keep="http_proxy"' | sudo tee -a /etc/sudoers
sudo apt-get update


#java
sudo apt-get -y install openjdk-7-jdk

#make
sudo apt-get -y install make

#redis
cd /home/vagrant
mkdir redis
cd redis
wget http://download.redis.io/redis-stable.tar.gz
tar xvzf redis-stable.tar.gz
cd redis-stable
make

#solr
cd /home/vagrant
mkdir solr
cd solr
wget http://ftp.ps.pl/pub/apache/lucene/solr/4.9.0/solr-4.9.0.tgz
tar zxvf solr-4.9.0.tgz
cd solr-4.9.0/
cp -r example/ shard1/
cd shard1
cp -r solr/collection1/ solr/recsys/
rm -rf solr/recsys/data/
find . -name "core.properties" -type f -exec rm {} \;
echo "name=recsys" > solr/recsys/core.properties
sudo sed s/127.0.1.1/192.168.2.100/ /etc/hosts > /tmp/hosts
sudo cat /tmp/hosts > /etc/hosts
java -Dbootstrap_confdir=./solr/recsys/conf -Dcollection.configName=recsys -DzkRun -DnumShards=1 -jar start.jar &
sleep 20
killall java
cd /home/vagrant/solr/solr-4.9.0/
cp -r shard1 shard2

#rabbitmq
cd /home/vagrant
sudo echo "deb http://www.rabbitmq.com/debian/ testing main" | sudo tee -a /etc/apt/sources.list
mkdir rabbitmq
cd rabbitmq
wget http://www.rabbitmq.com/rabbitmq-signing-key-public.asc
sudo apt-key -y add rabbitmq-signing-key-public.asc
sudo apt-get -y install rabbitmq-server

sudo apt-get -y install python-pip git-core
sudo pip install pika==0.9.8

python << EOS
import pika

connection = pika.BlockingConnection(pika.ConnectionParameters(
               'localhost'))
channel = connection.channel()
channel.queue_declare(queue='hello2')
EOS

