#!/bin/bash
cd /home/vagrant/redis/redis-stable
./src/redis-server &
cd /home/vagrant/solr/solr-4.9.0/shard1
java -Dcollection.configName=recsys -DzkRun -DnumShards=2 -jar start.jar &
sleep 10
cd /home/vagrant/solr/solr-4.9.0/shard2
java -DzkHost=localhost:9983 -Djetty.port=8984 -jar start.jar &
